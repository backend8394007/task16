console.log('server is starting');

const express = require('express');

const app = express();

const server = app.listen(3000, listening = () =>{
    console.log("listening...");
});

app.use(express.static('website'));

app.get('/search/:car', sendCar = (request, response) =>{
    let data = request.params;
    response.send('The brand of the ' + data.car + ' car is cool');
});

app.get('/welcome/:name', sendWelcome = (request, response) =>{
    let data = request.params;
    response.send('Welcome ' + data.name + '! How are you?');
});